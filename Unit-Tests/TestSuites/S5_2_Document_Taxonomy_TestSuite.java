/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author RNagel
 */
public class S5_2_Document_Taxonomy_TestSuite extends BaseClass {

    static TestMarshall instance;

    public S5_2_Document_Taxonomy_TestSuite() {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.coreBeta;
    }

    //S5_2_Document_Taxonomy_QA01S5_2
    @Test
    public void S5_2_Document_Taxonomy_QA01S5_2() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - v5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\S5_2_Document_Taxonomy_QA01S5_2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR1-Capture Document Taxonomy - Main Scenario
    @Test
    public void FR1_Capture_Document_Taxonomy_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - v5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Document Taxonomy v5.2\\FR1-Capture Document Taxonomy - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR1_Document_Taxonomy_Optional_Scenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - v5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Document Taxonomy v5.2\\FR1-Capture Document Taxonomy - Optional Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR2-Capture Document Taxonomy Level 2 - Main Scenario
    @Test
    public void FR2_Capture_Document_Taxonomy_Level_2_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - v5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Document Taxonomy v5.2\\FR2-Capture Document Taxonomy Level 2 - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
      
    //FR3-Capture Document Taxonomy Level 3 - Main Scenario
    @Test
    public void FR3_Capture_Document_Taxonomy_Level_3_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - v5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Document Taxonomy v5.2\\FR3-Capture Document Taxonomy Level 3 - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
}
