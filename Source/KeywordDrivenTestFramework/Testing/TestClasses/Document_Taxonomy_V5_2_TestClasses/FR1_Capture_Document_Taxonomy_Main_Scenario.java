/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Document_Taxonomy_V5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Document_Taxonomy_V5_2_PageObjects.Document_Taxonomy_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;


/**
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture Document Taxonomy v5.2 - Main Scenario",
        createNewBrowserInstance = false
)
public class FR1_Capture_Document_Taxonomy_Main_Scenario  extends BaseClass{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR1_Capture_Document_Taxonomy_Main_Scenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!Navigate_To_Document_Taxonomy()) {
            return narrator.testFailed("Navigate To Document Taxonomy Failed due - " + error);
        }
        if (!captureDocumentTaxonomy()) {
            return narrator.testFailed("Document Management Page Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Captured Document Taxonomy Record");
    }

    public boolean Navigate_To_Document_Taxonomy() {
        //Navigate to Environmental Health & Safety
        if (!SeleniumDriverInstance.waitForElementByXpath(Document_Taxonomy_PageObjects.navigate_EHS())) {
            error = "Failed to wait for 'Environmental, Health & Safety' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Document_Taxonomy_PageObjects.navigate_EHS())) {
            error = "Failed to click on 'Environmental, Health & Safety' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' button.");

        //Navigate to Leadership & Participation Maintenance
        if (!SeleniumDriverInstance.waitForElementByXpath(Document_Taxonomy_PageObjects.LeaderParticipationMaintenance())) {
            error = "Failed to wait for 'Leadership & Participation Maintenance' module.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Document_Taxonomy_PageObjects.LeaderParticipationMaintenance())) {
            error = "Failed to click on 'Leadership & Participation Maintenance' module.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Leadership & Participation Maintenance' module.");

        //Document Taxonomy
        if (!SeleniumDriverInstance.waitForElementByXpath(Document_Taxonomy_PageObjects.DocumentTaxonomy_Module())) {
            error = "Failed to wait for 'Document Taxonomy' module";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Document_Taxonomy_PageObjects.DocumentTaxonomy_Module())) {
            error = "Failed to click on 'Document Taxonomy' module";
            return false;
        }

        return true;
    }

    public boolean captureDocumentTaxonomy() {
        //Add button
       SeleniumDriverInstance.pause(4000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Document_Taxonomy_PageObjects.Button_Add())) {
            error = "Failed to wait for add button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Document_Taxonomy_PageObjects.Button_Add())) {
            error = "Failed to click add button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Document_Taxonomy_PageObjects.Process_Flow())) {
            error = "Failed to click process flow";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Document_Taxonomy_PageObjects.Process_Flow())) {
            error = "Failed to click process flow";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the process flow button.");

        //Document taxonomy
        if (!SeleniumDriverInstance.waitForElementByXpath(Document_Taxonomy_PageObjects.Document_Taxonomy_Text_Feild())) {
            error = "Failed to wait for Document Taxonomy input field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Document_Taxonomy_PageObjects.Document_Taxonomy_Text_Feild(), testData.getData("Document Taxonomy"))) {
            error = "Failed to enter '" + testData.getData("Document Taxonomy") + "'Document Taxonomy input field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered: '" + testData.getData("Document Taxonomy") + "' into Document Taxonomy field.");

        //Business unit
//        if (!SeleniumDriverInstance.waitForElementByXpath(Document_Taxonomy_PageObjects.Applicable_Business_Unit_Select_All())) {
//            error = "Failed to wait for applicable business unit select all.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Document_Taxonomy_PageObjects.Applicable_Business_Unit_Select_All())) {
//            error = "Failed to click applicable business unit select all.";
//            return false;
//        }

          if (!SeleniumDriverInstance.waitForElementByXpath(Document_Taxonomy_PageObjects.businessUnitValue(testData.getData("BusinessUnitDropdownValue"))))
        {
            error = "Failed to wait for Business Unit Dropdown value";
            return false;
        }
          
         if (!SeleniumDriverInstance.clickElementbyXpath(Document_Taxonomy_PageObjects.businessUnitValue(testData.getData("BusinessUnitDropdownValue"))))
        {
            error = "Failed to click Business Unit Dropdown value";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected business unit value");

        //Impact type
//        if (!SeleniumDriverInstance.waitForElementByXpath(Document_Taxonomy_PageObjects.Applicable_Impact_Types_Select_All())) {
//            error = "Failed to wait for applicable impact types select all.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Document_Taxonomy_PageObjects.Applicable_Impact_Types_Select_All())) {
//            error = "Failed to click applicable impact types select all.";
//            return false;
//        }
 
        if (!SeleniumDriverInstance.waitForElementByXpath(Document_Taxonomy_PageObjects.businessUnitValue(testData.getData("ImpactTypeDropdownValue"))))
        {
            error = "Failed to wait for Impact type Dropdown value";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Document_Taxonomy_PageObjects.businessUnitValue(testData.getData("ImpactTypeDropdownValue"))))
        {
            error = "Failed to click Impact type Dropdown value";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected impact type value");

        //Link
        if (!SeleniumDriverInstance.waitForElementByXpath(Document_Taxonomy_PageObjects.Link_Text_Feild())) {
            error = "Failed to enter link in the input field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Document_Taxonomy_PageObjects.Link_Text_Feild(), testData.getData("Link"))) {
            error = "Failed to enter '" + testData.getData("Link") + "' link input field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered: '" + testData.getData("Link") + "' into Link field.");

        //Taxonomy abbreviation
        if (!SeleniumDriverInstance.waitForElementByXpath(Document_Taxonomy_PageObjects.Taxonomy_Abbreviation_Text_Feild())) {
            error = "Failed to wait for Taxonomy Abbreviation input field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Document_Taxonomy_PageObjects.Taxonomy_Abbreviation_Text_Feild(), testData.getData("Taxonomy Abbreviation"))) {
            error = "Failed to enter '" + testData.getData("Taxonomy Abbreviation") + "' into Taxonomy Abbreviation input field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered: '" + testData.getData("Taxonomy Abbreviation") + "' into Taxonomy abbreviation field.");

        if (!SeleniumDriverInstance.clickElementbyXpath(Document_Taxonomy_PageObjects.Button_Save())) {
            error = "Failed to click button save";
            return false;
        }

        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(Document_Taxonomy_PageObjects.inspection_Record_Saved_popup())) {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Document_Taxonomy_PageObjects.inspection_Record_Saved_popup());
        } else {
            if (!SeleniumDriverInstance.waitForElementByXpath(Document_Taxonomy_PageObjects.inspection_Record_Saved_popup())) {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Document_Taxonomy_PageObjects.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Document_Taxonomy_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");


        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(Document_Taxonomy_PageObjects.docTaxonomyRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());    
         

        if (saved.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else {
            if (!SeleniumDriverInstance.waitForElementByXpath(Document_Taxonomy_PageObjects.failed())) {
                error = "Failed to wait for error message.";
                return false;
            }
            String failed = SeleniumDriverInstance.retrieveTextByXpath(Document_Taxonomy_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved")) {
                error = "Failed to save record.";
                return false;
            }
        }

        return true;
    }

}